require('dotenv').config();
const express = require('express');
const fetch = require('node-fetch');
var app = express();
const Album = require('./app/models/Album.model');

//Request User Authorization
app.get('/login', function (req, res) {
    res.redirect('https://accounts.spotify.com/authorize?response_type=code&redirect_uri=http%3A%2F%2Flocalhost%3A8001%2F&client_id=' + process.env.CLIENT_ID)
});

//Request Access Token
app.get('/', function (req, res) {

    var code = req.query.code;

    fetch('https://accounts.spotify.com/api/token?code=' + code + '&redirect_uri=http%3A%2F%2Flocalhost%3A8001%2F&grant_type=authorization_code', {
        method: "Post",
        headers: {
            'Authorization': 'Basic ' + (Buffer.from(process.env.CLIENT_ID + ':' + process.env.CLIENT_SECRET).toString('base64')),
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        json: true
    })
        .then(res => res.json())
        .then((data) => { this.acc_token = data.access_token; this.refresh_token = data.refresh_token })
        .then(() => {
            //Request a refreshed Access Token
            setInterval(() => {
                fetch('https://accounts.spotify.com/api/token?grant_type=refresh_token&refresh_token=' + this.refresh_token, {
                    method: "Post",
                    headers: {
                        'Authorization': 'Basic ' + (Buffer.from(process.env.CLIENT_ID + ':' + process.env.CLIENT_SECRET).toString('base64')),
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    json: true
                })
                    .then(data => this.acc_token = data.access_token)
            }, 3600000)
            //Get list of track and albums
            fetch("https://api.spotify.com/v1/artists/37M5pPGs6V1fchFJSgCguX/albums", {
                method: "GET",
                headers: {
                    Authorization: "Bearer " + this.acc_token
                }
            })
                .then((res) => res.json())
                .then((data) => this.albums = data.items)
                .then(() => {
                    const album = new Album(this.albums)
                    album.save()
                        .catch(error => {
                            console.log(error)
                        })

                })
                // .then(() => {
                //     for (let i = 0; i <= this.albums.length; i++) {
                //         fetch("https://api.spotify.com/v1/albums/" + this.albums[i].id + "/tracks", {
                //             method: "GET",
                //             headers: {
                //                 Authorization: "Bearer " + this.acc_token
                //             }
                //         })
                //             .then((res) => res.json())
                //             .then((data => {
                //                 const track = new Album(data)
                //                 track.save()
                //                     .catch(error => {
                //                         console.log(error)
                //                     })
                //             }
                //             ))
                //     }
                // })
        });
});

app.listen(8001, () => {
    console.log('listen on port 8001...')
});