const musicRouter = require('./music.route');
const siteRouter = require('./site.route');
const userRouter = require('./users.route');

function route(web) {

    web.use('/music', musicRouter);
    web.use('/user', userRouter);
    web.use('/', siteRouter);
}

module.exports = route;
