const express = require('express');
const router = express.Router();
const passport = require('passport');
const { check, validationResult } = require('express-validator');
const userController = require('../app/controllers/UserController');

router.get('/signin', userController.signin);
router.get('/signup', userController.signup);
router.post('/signup', passport.authenticate('local.signup', {
    successRedirect: '/user/signin',
    failureRedirect: '/user/signup',
    failureFlash: true,
}));
router.post('/signin', passport.authenticate('local.signin', 
// [
//     check('email', 'Your email is not valid').isEmail(),
//     check('password', 'Your password must be at least 5 characters').isLength({ min: 5 })
//     ],
//     (function (req, res, next) {
  
//     var messages = req.flash('error');
//     const result= validationResult(req);
//     var errors=result.errors;
//     if (!result.isEmpty()) {
//       var messages = [];
//       errors.forEach(function(error){
//           messages.push(error.msg);
//       });
//       res.render('signup',{
//         messages: messages,
//         hasErrors: messages.length > 0,
//       });
//     }else{
//        next();
//     }
//     }),
    {
    successRedirect: '/',
    failureRedirect: '/user/signin',
    failureFlash: true })
);

module.exports = router
