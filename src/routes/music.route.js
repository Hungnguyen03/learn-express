const express = require('express');
const router = express.Router();
const albumController = require('../app/controllers/AlbumController');
// const trackController = require('../app/controllers/TrackController');

router.get('/album/:id/edit', albumController.editAlbum);
router.get('/album/:id/edited', albumController.updateAlbum);
router.get('/album/create', albumController.createAlbum);
router.get('/album/store', albumController.storeAlbum);
router.get('/album/:id/delete', albumController.deleteAlbum);
// router.get('/album/:name', trackController.track);
router.get('/album', albumController.album);
router.get('/', albumController.index);

module.exports = router
