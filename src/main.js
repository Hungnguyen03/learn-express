const path = require('path')
const express = require('express');
const web = express();
const handlebars = require('express-handlebars');
const route = require('./routes/index.route.js');
const db = require('./config/db');
const passport = require('passport');
const session = require('express-session')
const flash = require('connect-flash');

require('./config/passport'); //vượt qua passport để config trang đăng nhâp/đăng ký
web.use(session({
  secret: 'adsa897adsa98bs',
  resave: false,
  saveUninitialized: false,
}))
web.use(flash());
web.use(passport.initialize())
web.use(passport.session());

//template engines
web.engine('hbs', handlebars({
    extname: '.hbs'
}));
web.set('view engine', 'hbs');
web.set('views', path.join(__dirname, 'views'))

web.use(require('body-parser').urlencoded({ extended: false }));
//Routes init
route(web);

//Connect to db
db.connect();

web.listen(3000,"0.0.0.0", () => {
    console.log('listen on port 3000...')
});
