const Album = require('../models/Album.model');
const { mutipleMongooseToObject } = require('../../util/mongoose');
const { mongooseToObject } = require('../../util/mongoose');

class AlbumController {

    index(req, res) {
        res.render('music');
    };

    album(req, res, next) {
        Album.find({})
            .then(albums => res.render('music/album', { albums: mutipleMongooseToObject(albums) }))
            .catch(error => console.log(error))
    };

    createAlbum(req, res, next) {
        res.render('music/create')
    };

    storeAlbum(req, res, next) {
        const album = new Album(req.query);
        album.save()
            .then(() => res.redirect('/music/album'))
            .catch(error => {
                console.log(error)
            })
    };

    editAlbum(req, res, next) {
        Album.findById(req.params.id)
            .then(albums => res.render('music/update', { albums: mongooseToObject(albums) }))
            .catch(error => console.log(error));
    }

    updateAlbum(req, res, next) {
        Album.findByIdAndUpdate(req.params.id, req.query)
            .then(() => res.redirect('/music/album'))
            .catch(error => {
                console.log(error)
            })
    }

    deleteAlbum(req, res, next) {
        Album.findByIdAndDelete(req.params.id)
            .then(() => res.redirect('/music/album'))
            .catch(error => {
                console.log(error)
            })
    }
}

module.exports = new AlbumController;
