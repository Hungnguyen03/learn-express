const User = require("../models/User.model");
const passport = require("passport");

class UserController {

    signin(req, res, next) {
        res.render('signin');
    }

    signup(req, res, next) {
        res.render('signup');
    }

}

module.exports = new UserController;
