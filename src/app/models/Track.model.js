const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Track = new Schema({
    name: { type: String, minlength: 1 },
    author: { type: String, default: 'Unknown' },
    album: { type: String },
}, {
    timestamps: true,
});

module.exports = mongoose.model('Track', Track);
