const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Album = new Schema({
  name: { type: String, minlength: 1 },
  author: { type: String, default: 'Unknown'},
  image: {},
  tracks: [
    {
      name: { type: String, minlength: 1 },
      time: {  },
      clickCount: { type: Number },
    }
  ]},
  {
    timestamps: true,
  });

module.exports = mongoose.model('Album', Album);
